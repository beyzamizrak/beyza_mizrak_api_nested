package pojo_classes;

import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class Meta {



    private Pagination pagination;
}
