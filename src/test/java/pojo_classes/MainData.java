package pojo_classes;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class MainData {

    private int code;
    private Meta meta;
    private List<DataRaw> dataRaw;

}
