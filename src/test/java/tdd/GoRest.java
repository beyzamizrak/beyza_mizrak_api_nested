package tdd;

import com.github.javafaker.Faker;
import io.cucumber.java.it.Ma;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.reporters.jq.Main;
import pojo_classes.*;
import utils.ConfigReader;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GoRest {

    Response response;

    Faker faker = new Faker();


    @BeforeTest
    public void beforeTest(){
        System.out.println("\n--------Api test is starting----------\n");
        RestAssured.baseURI = ConfigReader.getProperty("GoRestBaseURI");
    }

    @Test
    public void postTest(){

        Links links = Links.builder()
                .previous(faker.internet().url())
                .current(faker.internet().url())
                .next(faker.internet().url())
                .build();

        Pagination pagination = Pagination.builder()
                .total(1000)
                .pages(100)
                .page(10)
                .limit(1)
                .links(links)
                .build();

        Meta meta = Meta.builder()
                .pagination(pagination)
                .build();

        DataRaw data1 = DataRaw.builder()
                .id(1)
                .post_id(10)
                .name(faker.name().fullName())
                .email(faker.internet().emailAddress())
                .body(faker.internet().url())
                .build();
        DataRaw data2 = DataRaw.builder()
                .id(2)
                .post_id(20)
                .name(faker.name().fullName())
                .email(faker.internet().emailAddress())
                .body(faker.internet().url())
                .build();

        DataRaw data3 = DataRaw.builder()
                .id(3)
                .post_id(30)
                .name(faker.name().fullName())
                .email(faker.internet().emailAddress())
                .body(faker.internet().url())
                .build();

        List<DataRaw> dataRows = new ArrayList<>(Arrays.asList(data1, data2, data3));

        MainData mainData = MainData.builder()
                .code(200)
                .meta(meta)
                .dataRaw(dataRows)
                .build();

        response = RestAssured
                .given().log().all()
                .contentType(ContentType.JSON)
                .header("Authorization", ConfigReader.getProperty("GoRestToken"))
                .body(mainData)
                .when().post("public-api/comments")
                .then().log().all()
                .and().assertThat().statusCode(200)
                .extract().response();


        JsonPath jsonPath =response.jsonPath();
        assertThat(jsonPath.getList("data").get(0).toString(), Matchers.equalTo("{field=post, message=must exist}"));
        assertThat(jsonPath.getList("data").get(1).toString(), Matchers.equalTo("{field=post_id, message=is not a number}"));
        assertThat(jsonPath.getList("data").get(2).toString(), Matchers.equalTo("{field=name, message=can't be blank}"));
        assertThat(jsonPath.getList("data").get(3).toString(), Matchers.equalTo("{field=email, message=can't be blank, is invalid}"));





    }


}
